import bpy
import shared
from bpy import (data, context, ops)


# ------------------------------------------------------------------ #
# RUN BODY FIX
# ------------------------------------------------------------------ #
def fix_body(self, context):
	print("\n>>> START: BODY FIX")
	global C
	global armature
	global edit_bones	
	C = context
	
	# Find armature
	armature, status = shared.find_armature(self, context, "BODY")	
	if status != "RUN":
		print("Fixer status: " + status)
		return armature, status  # Exit if armature doesn't need fixing
	
	# ------------------------------------------------------------------ #	
	# RUN FIX
	print("BODY FIX: " + armature.name)
	edit_bones = armature.data.edit_bones	# Store list of bones in armature
	
	# OBJECT MODE
	shared.mode_object()
	C.scene.objects.active = armature  # Make armature the active object
	
	# Fix armature and loose objects
	accs = shared.fix_armature_objects(self, context, armature, "body", exclude_list)  # Fix object rotations and parenting
	
	# EDIT MODE
	shared.mode_edit()
	parent_bones()		# Re-parent roll bones to correct part
	merge_bones()		# Merged / delete excess bones
	resize_bones()		# Resize bones so tail meets the head of child bone
	rename_bones()		# Rename bones to match naming convention
	shared.add_weight(accs, edit_bones, False)	# Add weight to loose objects
	
	# OBJECT MODE	
	shared.mode_object()
	C.scene.objects.active = armature  # Make armature the active object
	
	# Add empty vertex group to hand, if its missing
	for child in armature.children:
		if "m_hand" in child.name:
			for x in ["Left", "Right"]:
				if child.vertex_groups.find(x + " wrist") == -1:
					child.vertex_groups.new(x + " wrist")
	
	# Finish fixer
	shared.finish_fixer(self, context, armature, "Body")
	return armature, status


# ------------------------------------------------------------------ #
# Re-parent roll bones to correct part
# ------------------------------------------------------------------ #
def parent_bones():
	shared.select_all_armature()
	edit_bones["Hips"].parent = None
	
	for bone in parent_list:
		name = bone[0]
		parent = bone[1]
		
		if edit_bones.find(name) != -1:
			edit_bones[name].parent =  edit_bones[parent]
	
	print("Fixed knee and elbow roll")
	

# ------------------------------------------------------------------ #
# Merge excess bones that aren't used by VRChat
# ------------------------------------------------------------------ #
def merge_bones():
	shared.select_none_armature()
	merge_list = []
	
	# Find bones to be to be merged
	for bone in edit_bones:
		bone_name = bone.name.lower()	# Lowercase bone name, easier for searching
		if bone_name == "spine":  								
			merge_list.append(bone.name)	# Append 'spine' bone to merge list
		else:
			if any(x in bone_name for x in ["_roll", "_ex"]):	# Append 'roll' and 'ex' bones
				merge_list.append(bone.name)
	
	# Merge weights of excess bones using CATS
	for x in merge_list:
		edit_bones[x].select = True	
	bpy.ops.cats_manual.merge_weights()
	shared.select_none_armature()
	print("Merged excess bones")
	
	# Remove excess bones
	edit_bones.remove(edit_bones["root"])				# Root bone
	shared.remove_mesh_bones(edit_bones)	 			# Mesh bones
	shared.remove_toe_bones(context, armature, "EDIT")	# Toe bones


# ------------------------------------------------------------------ #
# Resize and rename bones
# ------------------------------------------------------------------ #
def resize_bones():
	shared.select_none_armature()	
	edit_bones["Hips"].head.z = edit_bones["RightUpLeg"].head.z  # Only change vertical position
	edit_bones["Hips"].tail = edit_bones["Spine1"].head
	edit_bones["Spine1"].tail = edit_bones["Spine2"].head
	
	for x in ["Left", "Right"]:
		# Scale tail to match the head of child bone
		edit_bones[x + "Arm"].tail = edit_bones[x + "ForeArm"].head
		edit_bones[x + "ForeArm"].tail = edit_bones[x + "Hand"].head
		edit_bones[x + "UpLeg"].tail = edit_bones[x + "Leg"].head
		
	print("Resized bones")


# ------------------------------------------------------------------ #
# Rename bones to match naming convention
# ------------------------------------------------------------------ #
def rename_bones():	
	# Body
	for body in body_list:
		old_name = body[0]
		new_name = body[1]
		# Make sure bone exists
		if edit_bones.find(old_name) != -1:
			edit_bones[old_name].name = new_name
	
	# Arms & Legs
	for x in ["Left", "Right"]:
		for limb in limb_list:
			old_name = x + limb[0]
			new_name = x + limb[1]
			
			if edit_bones.find(old_name) != -1:
				edit_bones[old_name].name = new_name  # Rename if bone exists		
		
		# Hands
		for int in [1, 2, 3]:
			i = str(int)
			j = str(int - 1)
			suffix = x[:1]
			
			# Thumb - e.g. LeftHandThumb1 --> Thumb0_L
			old_name = x + "HandThumb" + i
			new_name = "Thumb" + j + "_" + suffix
			
			if edit_bones.find(old_name) != -1:
				edit_bones[old_name].name = new_name  # Rename if bone exists
			
			# Fingers - e.g. LeftHandIndex1 --> IndexFinger1_L
			for finger in hand_list:
				old_name = x + finger[0] + i
				new_name = finger[1] + i + "_" + suffix	
				
				if edit_bones.find(old_name) != -1:
					edit_bones[old_name].name = new_name  # Rename if bone exists
		
	print("Renamed bones")


# ------------------------------------------------------------------ #
# Lists of old and new bone names
# ------------------------------------------------------------------ #
body_list = [["head", "Head"],
			 ["neck", "Neck"],
			 ["Spine1", "Spine"],
			 ["Spine2", "Chest"],
			 ]
						
limb_list = [["Shoulder", " shoulder"],
			 ["Arm", " arm"],
			 ["ForeArm", " elbow"],
			 ["Hand", " wrist"],
			 ["Leg", " knee"],  # Knee must be before leg to avoid conflicting names
			 ["UpLeg", " leg"],
			 ["Foot", " ankle"],
			 ["ToeBase", " toe"],
			 ]
						
hand_list = [["HandIndex", "IndexFinger"],
			 ["HandMiddle", "MiddleFinger"],
			 ["HandRing", "RingFinger"],
			 ["HandPinky", "LittleFinger"],
			 ]
	
parent_list = [["L_Knee_Roll_01", "LeftLeg"],
			   ["R_Knee_Roll_01", "RightLeg"],
			   ["L_Elbow_Roll", "LeftForeArm"],
			   ["R_Elbow_Roll", "RightForeArm"],
			   ["L_EXarm", "LeftArm"],
			   ["R_EXarm", "RightArm"],
			   ]

exclude_list = ["face",
				"eye",
				"blow", 
				"mouth", 
				"sirome", 
				"hair", 
				"headp", 
				"h00", 
				"hat",
				]