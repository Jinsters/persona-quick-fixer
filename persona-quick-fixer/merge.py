import bpy
import body
import face
import hair
import shared
import eyetracking

from bpy import (data, context, ops)
from body import *
from hair import *
from face import *
from eyetracking import *

#TODO Match head height to hair_root?

# ------------------------------------------------------------------ #
# MERGE ARMATURES
# ------------------------------------------------------------------ #	
def merge_armatures(self, context):
	print("\n>>> START: MERGE ARMATURES")

	# Find armatures
	global body, face, hair, armature
	body, status_body = fix_body(self, context)
	face, status_face = fix_face(self, context)
	hair, status_hair = fix_hair(self, context)
		
	if body == face == hair == None:
		if "MULTI" in (status_body, status_face, status_hair):
			# Multiple armatures
			self.report({"ERROR"}, "Multiple armatures found \nMake sure there is only one of each body part")
		else:
			# No armatures
			self.report({"ERROR"}, "No armatures found")
		return {"CANCELLED"}

	print("\nCONTINUE MERGE ARMATURES")
	shared.select_none()
	# armature = body if (body is not None) else face	 # Use body as primary armature, else use face
	armature = face if (face is not None) else body	 	# Use face as primary armature, else use body
	print("Primary armature: " + armature.name)
	
	# Find primary head bone
	head_bone = find_head_bone()
	if not head_bone:
		self.report({"ERROR"}, "Head bone missing from: " + armature.name)
		return False
	
	fix_hair_bones(context)		 # Rename 'Hair_Root' and remove neck bone from hair armature
	shared.set_active(armature)  # Set primary armature as active object
	
	# Apply transform of all objects
	print("Applying transforms to all objects...")
	shared.select_none()
	arm_list = []

	for arm in filter(lambda y: y is not None, [body, face, hair]):
		arm_list.append(arm)		 # Add armature to list
		arm.select = True			 # Select armature
		print("Selecting children in: " + arm.name)
		for child in arm.children:
			child.select = True	 # Select all children
			# print("Selected: " + child.name)
	
	ops.object.transform_apply(location=True, rotation=True, scale=True)  # Apply transform
	print("Transform applied to all selected objects")
	shared.select_none()

	# Join armatures
	offset = armature.vrc_offset						# Store offset property of primary armature
	for x in arm_list: x.select = True					# Re-select armatures
	bpy.ops.object.join()		 						# Join armatures
	print("Merged armatures to: " + armature.name)

	# Offset armature to prevent feet sinking
	if context.scene.fix_feet:
		shared.mode_object()							# Object Mode
		armature.location.z = offset					# Offset by toe height or default (8cm)
		print("Armature offset by: " + str(offset))
	
	# Finalise armature
	fix_armature_deform()								# Set all objects to use correct armature deform modifier
	fix_hierarchy()										# Reparent bones - Edit Mode
	shared.finish_fixer(self, context, armature)		# Join mesh / Remove outlines and rename armatures	
	shared.redo_cats(self, context, armature)			# Re-create eye-tracking and visemes, if stingle mesh
	shared.rename_armature(context, armature)			# Rename armature and set to pose position
	return True
	

# ------------------------------------------------------------------ #
# FUNCTIONS
# ------------------------------------------------------------------ #
# Find primary head bone
def find_head_bone():
	for bone in filter(lambda x: x.name=="Head", armature.data.bones):
		return bone


# Merge bones using CATS - EDIT MODE
def merge_with_cats(arm, bone, parent):
	bpy.ops.armature.select_all(action='DESELECT')
	bone.parent = arm.data.edit_bones[parent]
	bone.select = True
	bpy.ops.cats_manual.merge_weights()
	print("Merged '" + bone.name + "' to '" + parent + "' bone")	
	
	
# Rename 'Hair_Root' and remove neck bone from hair armature
def fix_hair_bones(context):
	if hair is not None:  			# Find head and neck bones in hair
		shared.set_active(hair)		# Set hair as active object
		shared.mode_edit()
		edit_bones = hair.data.edit_bones
	
		for bone in edit_bones:
			if bone.name == "Head":
				bone.name = "Hair_Root"
				print("Renamed 'Hair_Root' bone")	# Rename head bone to "Hair_Root"

	shared.mode_object()


# Fix armature deform
def fix_armature_deform():
	for obj in armature.children:
		for mod in filter(lambda x: x.type == "ARMATURE", obj.modifiers):
			mod.object = armature
	print("Set Armature Deform: " + armature.name)
	

# Re-parent and merge excess bones
def fix_hierarchy():
	shared.mode_edit()
	edit_bones = armature.data.edit_bones

	for bone in edit_bones:
		# Parent head to neck (if un-parented)
		if bone.name == "Head" and bone.parent is None:
			bone.parent = edit_bones["Neck"]
			print("Parented 'Head' to 'Neck'")

		# Parent neck to chest (if un-parented)
		if bone.name == "Neck" and bone.parent is None:
			bone.parent = edit_bones["Chest"]
			print("Parented 'Neck' to 'Chest'")

		# Parent hair root bone to head
		elif "Hair_Root" in bone.name:
			bone.parent = edit_bones["Head"]
			print("Parented 'Hair_Root' to 'Head'")

		# Remove excess head bone
		elif "Head." in bone.name:
			merge_with_cats(armature, bone, "Head")

		# Remove excess neck bone
		elif "Neck." in bone.name:
			merge_with_cats(armature, bone, "Neck")
	
	shared.mode_object()