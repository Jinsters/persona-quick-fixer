import bpy
import os
import shared
from bpy import (data, context, ops, path)
from collections import OrderedDict

# ------------------------------------------------------------------ #
# Export textures from selected objects
# ------------------------------------------------------------------ #
def texture_export(self, context):
	relative_directory = None
	list_image = []
	list_export = ["Exported successfully:"]
	list_skip = ["Skipped because file already exists:"]
	list_fail = ["Failed to export:"]
	
	# Get directory of selected folder
	filepath = path.abspath(self.filepath)
	directory = os.path.dirname(filepath) + "\\"
	
	# Get relative path (optional)
	if self.is_relative:
		if path.abspath("//") != "": 
			relative_directory = path.relpath(directory) + "\\"	 # Check blend file has a filepath
	
	print("Export to -> " + directory)
	print("Getting textures from visible objects...")
	
	# Get textures from visible objects
	for obj in context.visible_objects:
		if obj.type == "MESH":
			for mat in obj.data.materials:
				for tex in mat.texture_slots:
					if tex is None:			
						continue					# Material has no texture
					else:
						img = tex.texture.image		# Get texture image
						if img not in list_image:
							list_image.append(img)	# Store image in list
	
	if list_image == []:
		self.report({"ERROR"}, "No textures found")
		return False
	
	# Export images
	print("Exporting texture images as PNG...")
	for image in list_image:		
		image_name = get_name(image)					# Get image name as PNG
		found = os.path.exists(directory + image_name)	# Check for existing image in destination

		if found and self.do_overwrite is False:
			list_skip.append(image_name)								# Existing image found
		else:
			try:
				image.save_render(filepath = directory + image_name)	# Save image
				list_export.append(image_name)		
			except RuntimeError:
				list_fail.append(image_name)							# Failed to export
				continue

		if self.update_tex is True:									# If option selected...
			if relative_directory is None:	
				image.filepath = directory + image_name				# Update texture to use PNG image		
			else:							
				image.filepath = relative_directory + image_name	# Use relative path if available
	
	message = OrderedDict()

	if len(list_export) > 1:
		message["FILE_TICK"] = list_export		# Exported successfully

	if len(list_skip) > 1:
		message["LOOP_FORWARDS"] = list_skip	# Skipped images the already existed
	
	if len(list_fail) > 1:
		message["ERROR"] = list_fail			# Failed to export
	
	# Display messagebox
	shared.messagebox_multi(self, title="Texture Export", 
							   	  message=message,
							   	  popup=True,
							   	  )

	print(">>> FINISHED: EXPORT TEXTURES")
	return True

# ------------------------------------------------------------------ #
# Get image name as PNG
# ------------------------------------------------------------------ #
def get_name(image):	
	img = bpy.path.basename(image.filepath)	# Get full image name	
	ext = img.split(".")[-1]				# Get file extention	
	name = img.replace(ext, "png")			# Set image name with .png extension	
	return name