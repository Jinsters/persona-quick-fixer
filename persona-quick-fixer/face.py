import bpy
import re  # regex
from bpy import (data, context, ops)
from math import radians
from mathutils import Vector

import shared
import eyetracking
from eyetracking import create_eye_tracking

# ------------------------------------------------------------------ #
# RUN FACE FIX
# ------------------------------------------------------------------ #	
def fix_face(self, context):
	print("\n>>> START: FACE FIX")
	global C
	global armature
	global edit_bones	
	C = bpy.context	
	
	# Find armature
	armature, status = shared.find_armature(self, context, "FACE")
	if status != "RUN":
		print("Fixer status: " + status)
		return armature, status  # Exit if armature doesn't need fixing
	
	# ------------------------------------------------------------------ #	
	# RUN FIX
	print("FACE FIX: " + armature.name)
	edit_bones = armature.data.edit_bones	# Store list of bones in armature
		
	# Set variables
	string = ["face", "eye", "blow", "mouth", "sirome"]
	morph = []
	character_id = None	

	# Rotate and scale face
	for obj in context.visible_objects:		
		if obj.name.startswith("m_") and obj.parent is None:	# Check for loose mesh objects			
			if "MorphTarget" in obj.name:						# Store morph targets for deletion
				morph.append(obj)
			
			elif any(str in obj.name for str in string):	# If mesh name contains any of the specified strings...				
				shared.fix_rotation(obj)					# Fix rotation and scale				
				if "Leye" in obj.name:						# Determine character ID using the texture from left eye
					character_id = get_name(obj)					

	ops.object.parent_set(type="ARMATURE")	# Parent selection objects to armature
	shared.armature_visible(armature)		# Set armature to wireframe and x-ray
	shared.apply_transform(armature)		# Apply rotation and scale to selected objects
	shared.select_none()
	
	print("Armature fixed")
	
	# Delete morph targets
	if len(morph) > 0:
		for m in morph:
			m.select = True				
		ops.object.delete(use_global = False)			
		print("Morph targets deleted")

	# Fix bones
	shared.mode_edit()
	ops.armature.select_all(action='SELECT')	
	
	edit_bones = armature.data.edit_bones
	edit_bones["head"].tail.z = edit_bones["head"].head.z + 0.25
	edit_bones["L_eye"].tail.x = edit_bones["L_eye"].head.x + 0.03
	edit_bones["R_eye"].tail.x = edit_bones["R_eye"].head.x - 0.03
	
	for x in ["head", "L_eye", "R_eye"]:		
		if x != "head":
			edit_bones[x].tail.y = edit_bones[x].head.y - 0.15
			edit_bones[x].tail.z = edit_bones[x].head.z
			
		edit_bones[x].select = False
		edit_bones[x].name = edit_bones[x].name.capitalize()  # Capitalise bone names
		
	ops.armature.delete()
	ops.armature.calculate_roll(type='GLOBAL_POS_X')
	shared.mode_object()
	
	print("Bones fixed")	
	
	fix_mitsuru(character_id)	# Fix face position for Mitsuru
	
	# ------------------------------------------------------------------ #
	# RENAME SHAPE KEYS
	print("Removing prefix from shape keys...")
	shared.select_none()	
	
	# Search mesh objects in armature
	for obj in filter(lambda x: x.type == "MESH", armature.children):
		sk = obj.data.shape_keys
		if sk is not None:
			# Search each shape key in mesh
			for key in sk.key_blocks:
				# Use regex to find prefix in shape key
				match = re.search("m_.*_Mesh.*_MorphTarget", key.name)
				if match is not None:
					# Rename shapeykey with prefix removed
					prefix = match.group(0)								
					key.name = key.name.replace(prefix, "")
	
	print("Renamed shape keys")

	# ------------------------------------------------------------------ #
	# FIX EYES
	# Create eye vertex groups
	mesh_total = len(armature.children)	
	eye_r = find_face_mesh(string = "m_Reye_Mesh" + "1")
	eye_l = find_face_mesh(string = "m_Leye_Mesh" + "1")
	
	if eye_r is None or eye_l is None:		
		shared.messagebox(self, title="Error", 
								message="Eye mesh not found. Script cancelled!", 
								report="ERROR")
		return {"CANCELLED"}
	
	shared.add_vertex(context, eye_l, "L_eye")
	shared.add_vertex(context, eye_r, "R_eye")
		
	# Join eye and rename
	eye_r.select = True
	eye_l.select = True
	C.scene.objects.active = eye_r
	ops.object.join()
	eye_mesh = eye_r
	eye_mesh.name = "Eyes"

	# Delete eye shape keys
	C.scene.objects[eye_mesh.name].active_shape_key_index = 1
	ops.object.shape_key_remove(all=True)

	print("Removed eye shape keys")
	shared.select_none()

	# ------------------------------------------------------------------ #
	# FIX FACE MESH
	face_mesh, face_outline, eye_shadow = join_face_mesh(string = "m_face_Mesh", end = mesh_total)
	face_brow = find_face_mesh(string = "m_blow_Mesh" + "1")
	face_sirome = find_face_mesh(string = "m_sirome_Mesh" + "1")

	# Rename face shape keys
	for x in [eye_shadow, face_outline, face_mesh]:
		if x is not None:  # Skip outline
			sk = C.scene.objects[x.name].data.shape_keys.key_blocks
			sk["1"].name = "Blink.L"
			sk["2"].name = "Blink.R"
			sk["3"].name = "Happy"
			sk["4"].name = "Wink.L"
			sk["5"].name = "Wink.R"
			sk["6"].name = "Confused"
			sk["7"].name = "Surprised"
			sk["8"].name = "Angry"
			sk["9"].name = "Sad"
			sk["10"].name = "Bored"

	# Rename eyebrow shape keys
	sk = face_brow.data.shape_keys.key_blocks
	sk["6"].name = "Happy"
	sk["7"].name = "Surprised"
	sk["8"].name = "Angry"
	sk["9"].name = "Sad"
	sk["10"].name = "Bored"
	sk["11"].name = "Confused"

	print("Renamed face shape keys")

	#Join face, eyeblows and eyeballs
	face_brow.select = True
	face_sirome.select = True
	ops.object.join()

	print("Joined top face mesh: " + face_mesh.name)
	
	# Delete unnamed shape keys
	for x in [face_outline, face_mesh]:
		if x is not None:  # Skip if no outline
			C.scene.objects.active = x
			
			# Get list of shape keys in object
			sk = C.scene.objects[x.name].data.shape_keys.key_blocks.keys()
			
			# Check shape keys names for numbers
			for num in range(len(sk)):			
				i = x.data.shape_keys.key_blocks.find(str(num))
				
				# If shape key name is a number > Delete
				if i != -1:						
					context.object.active_shape_key_index = i
					ops.object.shape_key_remove(all=False)						
	
	print("Removed unnamed face shape keys")

	# Create single Blink
	face_mesh.select = True
	C.scene.objects.active = face_mesh
	
	for x in [face_outline, face_mesh]:
		if x is not None:  # Skip outline
			C.scene.objects.active = x
			
			# Set Blink values to 1	
			for sk in ["Blink.L", "Blink.R"]:
				x.data.shape_keys.key_blocks[sk].value = 1
				
			# Create new blink shape key
			x.shape_key_add(name = "Blink", from_mix = True)
			x.show_only_shape_key = True
			ops.object.shape_key_clear()
			x.active_shape_key_index = 0  # Select Basis

	print("Added 'Blink' shape key")
	shared.select_none()

	# ------------------------------------------------------------------ #
	# FIX MOUTH MESH
	mouth_mesh, mouth_outline, unused_var = join_face_mesh(string = "m_mouth_Mesh", end = mesh_total)

	# Rename mouth shape keys
	for x in [mouth_outline, mouth_mesh]:
		if x is not None:  # Skip if no outline
			sk = C.scene.objects[x.name].data.shape_keys.key_blocks
			sk["1"].name = "Ch"
			sk["2"].name = "Ou"
			sk["3"].name = "Oh"
			sk["4"].name = ":)"
			sk["5"].name = ":D"
			sk["6"].name = ":("
			# sk["7"].name = "Ah"  # ???
			sk["8"].name = "Grr"
			sk["9"].name = "Grin"
			sk["10"].name = ":|"

	print("Renamed mouth shape keys")
	
	# ------------------------------------------------------------------ #
	# CREATE VISEMES
	# Merge mouth to face + rename mesh
	mesh_face = join_face_single(face = [mouth_mesh, face_mesh])
	mesh_outline = join_face_single(face = [mouth_outline, face_outline])	
	face_mesh.name = "Face"
	if face_outline is not None: face_outline.name = "Face Outline"
	
	print("Merged mouth to face + renamed mesh")
	
	# Add weight to any extra mesh objecs, e.g. glasses
	for extra in armature.children:
		if extra not in (face_mesh, face_outline, eye_mesh):
			shared.add_vertex(context, extra, "Head")
	
	print("Added weight paint to accessories")
	
	# Fix Materials
	ops.cats_material.combine_mats()
	shared.fix_materials(context, armature)
	
	# ------------------------------------------------------------------ #
	# CREATE VISEMES -- Using CATS
	for mesh in [mesh_face, mesh_outline]:
		create_cats_visemes(self, context, armature, mesh)

	# CREATE EYE TRACKING -- Using internal module
	create_eye_tracking(self, context, armature, eye_mesh)
	
	# ------------------------------------------------------------------ #
	# Fix face position for Elizabeth and Margaret
	for mesh in [face_mesh, face_outline, eye_shadow]:
		if mesh is not None:
			fix_velvet(mesh, character_id)
	
	# FINISH FIXER
	shared.finish_fixer(self, context, armature, "Face")
	return armature, status


# ------------------------------------------------------------------ #
# VISEME FUNCTIONS
# ------------------------------------------------------------------ #
# Create visemes usings CATS
# Must use "bpy" to fix _RestrictContent error <<<
def create_cats_visemes(self, context, arm, mesh):
	shared.set_cats_armature(arm)	# Set armature in CATS

	if mesh is not None:
		print("Creating visemes using CATS for: " + mesh.name)
		set_cats_visemes(context, mesh)
		bpy.ops.cats_viseme.create()
		bpy.data.objects[mesh.name].show_only_shape_key = True		# Pin shape key

	shared.messagebox(self, title="Info", message="Created Visemes")	# Post message


# Set CATS visemes options to correct values
def set_cats_visemes(context, mesh):
	context.scene.mesh_name_viseme = mesh.name
	context.scene.mouth_a = ':D'
	context.scene.mouth_o = 'Oh'
	context.scene.mouth_ch = 'Ch'
	context.scene.shape_intensity = 1.0

	
# ------------------------------------------------------------------ #
# MESH FUNCTIONS
# ------------------------------------------------------------------ #
# Search armature for meshes starting with "m_"
def find_face_mesh(string):
	for c in armature.children:
		if c.name == string:
			return c		
		else:
			for i in range(20): # ARBITARY RANGE
				m = string + "." + str(i).zfill(3)			
				if c.name == m:
					return bpy.data.objects[m]	
	return None

# ------------------------------------------------------------------ #
# Join meshes
def join_face_mesh(string, end):
	mesh = None
	outline = None
	eye_shadow = None
	
	for i in range(end):
		x = find_face_mesh(string = string + str(i))
		
		if x is not None:
			for mat in x.data.materials:
				if mat.name.find("outline") != -1: # store outline
					outline = x
					
				elif mat.name.find("eye_sh") != -1: # store eye shadow & rename
					eye_shadow = x
					eye_shadow.name = "Eye Shadow"
					
				else: # select mesh
					mesh = x
					mesh.select = True
					C.scene.objects.active = mesh
	
	ops.object.join()
	print(">>> Joined mesh: " + mesh.name)
	return mesh, outline, eye_shadow

# ------------------------------------------------------------------ #
# Join face and mouth mesh
def join_face_single(face):
	shared.select_none()
	for mesh in face:
		if mesh is not None:
			mesh.select = True
			C.scene.objects.active = mesh
			shared.add_vertex(context, mesh, "Head")  # Add vertex group for "Head" bone
			
	if mesh is not None:
		ops.object.join()
		print(">>> Merged mouth --> face mesh: " + mesh.name)				
	return mesh


# ------------------------------------------------------------------ #
# FIX POSITIONING
# ------------------------------------------------------------------ #
# Get character name
def get_name(mesh):
	pass
	basename = bpy.path.basename
	material = mesh.material_slots[0].material
	texture = material.texture_slots[0].texture
	filepath = texture.image.filepath	
	filename = basename(filepath)	
	id = filename.split("_",1)[0].replace("pc","")	# Split filename at "_", take first group and remove "pc"	
	return id										# Return 3 digit character ID, e.g. Makoto = 101

# Fix face & eyes for Mitsuru
def fix_mitsuru(id):
	if id == "105":
		shared.select_none()
		
		for obj in armature.children:
			# Eyes
			if any(mesh in obj.name for mesh in ["eye", "sirome"]):
				obj.location.xyz = Vector((0.0, 0.0533141, 3.77655))			
				obj.rotation_euler = [0, radians(-90), radians(90)]
				obj.select = True
				
		# Apply rotation and scale to selected objects
		ops.object.transform_apply(location=True, rotation=True, scale=True)
		print("Fixed face position for Mitsuru")


# Fix face Elizabeth and Margaret
def fix_velvet(obj, id):
	shared.mode_object()
	shared.select_none()
	
	if id in ["111", "116"]:
		# Elizabeth
		if id == "111":		
			obj.location.xyz = Vector((0.0, 0.0, 0.18034))
		# Margaret
		elif id == "116":
			obj.location.xyz = Vector((0.0, -0.00490219, -0.120846))
		
		obj.select = True
		ops.object.transform_apply(location=True, rotation=True, scale=True)
		print("Fix face position for Elizabeth/Margaret")