import bpy
import shared
from bpy import (data, context, ops)
from mathutils import Vector


# ------------------------------------------------------------------ #
# RUN HAIR FIX
# ------------------------------------------------------------------ #	
def fix_hair(self, context):
	print("\n>>> START: HAIR FIX")
	global edit_bones
	
	# Find armature
	armature, status = shared.find_armature(self, context, "HAIR")	
	if status != "RUN":
		print("Fixer status: " + status)
		return armature, status  # Exit if armature doesn't need fixing
	
	# ------------------------------------------------------------------ #	
	# RUN FIX
	print("HAIR FIX: " + armature.name)
	edit_bones = armature.data.edit_bones	# Store list of bones in armature
	
	# Fix armature and loose objects
	accs = shared.fix_armature_objects(self, context, armature, "hair")  # Fix object rotations and parenting
	
	# EDIT MODE
	shared.mode_edit()
	rename_bones()			 					# Rename "Head" & "Neck" bones
	shared.remove_mesh_bones(edit_bones)		# Remove mesh bones
	shared.add_weight(accs, edit_bones, False)	# Add weight to loose objects
	
	# OBJECT MODE
	shared.finish_fixer(self, context, armature, "Hair")  # Finish fixer
	return armature, status
	
# ------------------------------------------------------------------ #
# Rename 'Head' & 'Neck' bones
# ------------------------------------------------------------------ #
def rename_bones():
	for x in ["head", "neck"]:
		bone = edit_bones[x]
		if bone is not None:
			new_name = x.capitalize()
			bone.name = new_name
			print("Renamed '" + new_name + "' bone")