# This pretty much copy pasted from Cats eyetracking module.
# I cut out quite a few operations which I don't THINK are need for persona models. Including: 
# - Tests that to make sure Cats runs correctly, e.g. bone hierarchy = hips, spine, etc.
# - Weird fixes like modifying shape keys to fix bugs in vrchat
#
# Copyright (c) 2017 GiveMeAllYourCats

import bpy
import shared
from bpy import (data, context, ops)
from mathutils import Vector


# ------------------------------------------------------------------ #
# CREATE EYE TRACKING
# ------------------------------------------------------------------ #
def set_cats_eyes(context, eyes):		
	# Prepare CATS eye tracking settings
	s = context.scene
	s.mesh_name_eye = eyes.name
	s.head = 'Head'
	s.eye_left = "L_eye"
	s.eye_right = "R_eye"
	s.eye_distance = 0.5
	s.disable_eye_blinking = True
	s.disable_eye_movement = False


def create_eye_tracking(self, context, arm, eyes):
	global armature
	armature = arm
	shared.set_active(armature)  # Set armature as active object
	
	# Set CATS to correct values	
	set_cats_eyes(context, eyes)		# Prepare CATS settings
	shared.set_cats_armature(armature)	# Set armature in CATS	
	shared.mode_edit()
	
	# Remove existing vertex groups to avoid duplicates
	remove_eye_vertex_groups(eyes)

	# Set up old bones
	head = armature.data.edit_bones.get(context.scene.head)
	old_eye_left = armature.data.edit_bones.get(context.scene.eye_left)
	old_eye_right = armature.data.edit_bones.get(context.scene.eye_right)
	
	# Set head roll to 0 degrees
	bpy.context.object.data.edit_bones[context.scene.head].roll = 0

	# Create the new eye bones
	new_left_eye = bpy.context.object.data.edit_bones.new('LeftEye')
	new_right_eye = bpy.context.object.data.edit_bones.new('RightEye')

	# Parent them correctly
	new_left_eye.parent = bpy.context.object.data.edit_bones[context.scene.head]
	new_right_eye.parent = bpy.context.object.data.edit_bones[context.scene.head]

	# Calculate their new positions
	fix_eye_position(context, old_eye_left, new_left_eye, head, False)
	fix_eye_position(context, old_eye_right, new_right_eye, head, True)

	# Switch to mesh
	mesh_name = context.scene.mesh_name_eye
	shared.mode_object()
	shared.set_active(bpy.data.objects[mesh_name])  # Set mesh as active object

	# Fix a small bug
	bpy.context.object.show_only_shape_key = False

	# Copy the existing eye vertex group to the new one if eye movement is activated
	if not context.scene.disable_eye_movement:
		copy_vertex_group(self, mesh_name, old_eye_left.name, 'LeftEye')
		copy_vertex_group(self, mesh_name, old_eye_right.name, 'RightEye')
	else:
		# Remove the vertex groups if eye movement is disabled
		mesh = bpy.data.objects[mesh_name]
		bones = ['LeftEye', 'RightEye']
		for bone in bones:
			group = mesh.vertex_groups.get(bone)
			if group is not None:
				mesh.vertex_groups.remove(group)
	
	# Create new eye shape keys to prevent default VRChat blink
	create_blink(eyes)
	print("Disabled default VRChat blinking")	
	
	# Reset eye tracking values
	set_cats_eyes(context, eyes)
	context.scene.eye_mode = "TESTING"
	shared.messagebox(self, title="Info", message="Created eye tracking using internal module")  # Post message


def remove_eye_vertex_groups(eyes):	
	# Remove existing eye vertex groups to avoid duplicates
	vg = eyes.vertex_groups

	for eye in ["LeftEye", "RightEye"]:
		index = vg.find(eye)
		if index != -1:
			vg.remove(vg[index])
			print("Removed vertex group: " + eye)
	

def fix_eye_position(context, old_eye, new_eye, head, right_side):
	# Verify that the new eye bone is in the correct position
	# by comparing the old eye vertex group average vector location
	mesh_name = context.scene.mesh_name_eye
	scale = -context.scene.eye_distance + 1

	if not context.scene.disable_eye_movement:
		if head is not None:
			coords_eye = find_center_vector_of_vertex_group(mesh_name, old_eye.name)
		else:
			coords_eye = find_center_vector_of_vertex_group(mesh_name, new_eye.name)

		if coords_eye is False:
			return

		if head is not None:
			mesh = bpy.data.objects[mesh_name]
			p1 = mesh.matrix_world * head.head
			p2 = mesh.matrix_world * coords_eye
			length = (p1 - p2).length
			print(length)  # TODO calculate scale if bone is too close to center of the eye

	# dist = math.sqrt((coords_eye[0] - head.head[x_cord]) ** 2 + (coords_eye[1] - head.head[y_cord]) ** 2 + (coords_eye[2] - head.head[z_cord]) ** 2)
	# dist2 = np.linalg.norm(coords_eye - head.head)
	# dist3 = math.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2 + (p1[2] - p2[2]) ** 2)
	# dist4 = np.linalg.norm(p1 - p2)
	# print(dist)
	# print(dist2)
	# print(2 ** 2)
	# print(dist4)

	# Check if bone matrix == world matrix
	# armature = tools.common.get_armature()
	x_cord = 0
	y_cord = 1
	z_cord = 2
	for index, bone in enumerate(armature.pose.bones):
		if index == 5:
			bone_pos = bone.matrix
			world_pos = armature.matrix_world * bone.matrix
			if abs(bone_pos[0][0]) != abs(world_pos[0][0]):
				z_cord = 1
				y_cord = 2
				break

	if context.scene.disable_eye_movement:
		if head is not None:
			if right_side:
				new_eye.head[x_cord] = head.head[x_cord] + 0.05
			else:
				new_eye.head[x_cord] = head.head[x_cord] - 0.05
			new_eye.head[y_cord] = head.head[y_cord]
			new_eye.head[z_cord] = head.head[z_cord]
	else:
		new_eye.head[x_cord] = old_eye.head[x_cord] + scale * (coords_eye[0] - old_eye.head[x_cord])
		new_eye.head[y_cord] = old_eye.head[y_cord] + scale * (coords_eye[1] - old_eye.head[y_cord])
		new_eye.head[z_cord] = old_eye.head[z_cord] + scale * (coords_eye[2] - old_eye.head[z_cord])

	new_eye.tail[x_cord] = new_eye.head[x_cord]
	new_eye.tail[y_cord] = new_eye.head[y_cord]
	new_eye.tail[z_cord] = new_eye.head[z_cord] + 0.1
	

def copy_vertex_group(self, mesh, vertex_group, rename_to):
	# iterate through the vertex group
	vertex_group_index = 0
	for group in bpy.data.objects[mesh].vertex_groups:
		# Find the vertex group
		if group.name == vertex_group:
			# Copy the group and rename
			bpy.data.objects[mesh].vertex_groups.active_index = vertex_group_index
			bpy.ops.object.vertex_group_copy()
			bpy.data.objects[mesh].vertex_groups[vertex_group + '_copy'].name = rename_to
			break

		vertex_group_index += 1


def find_center_vector_of_vertex_group(mesh_name, vertex_group):
	mesh = bpy.data.objects[mesh_name]

	data = mesh.data
	verts = data.vertices
	verts_in_group = []

	for vert in verts:
		i = vert.index
		try:
			mesh.vertex_groups[vertex_group].weight(i)
			verts_in_group.append(vert)
		except RuntimeError:
			# vertex is not in the group
			pass

	# Find the average vector point of the vertex cluster
	divide_by = len(verts_in_group)
	total = Vector()

	if divide_by == 0:
		return False

	for vert in verts_in_group:
		total += vert.co

	average = total / divide_by

	return average


# Create new eye shape keys to prevent default VRChat blink
def create_blink(eyes):
	new_shapes = ['vrc.blink_left', 'vrc.blink_right', 'vrc.lowerlid_left', 'vrc.lowerlid_right']
	add_key = eyes.shape_key_add	
	add_key(name = "Basis", from_mix=False)
	
	for blink in new_shapes:
		add_key(name = blink, from_mix=False)
	