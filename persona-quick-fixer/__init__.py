# MIT License

# Copyright (c) 2018 Jinsters

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# https://gitlab.com/Jinsters/persona-quick-fixer


bl_info = {
	"name": "Persona Quick Fixer",
	"author": "Jinsters",
	"version": (0, 5, 8),
	"blender": (2, 79, 0),
	"location": "View 3D > UI Panel > Persona Quick Fixer",
	"description": "A tool to fix Persona face models from P3D & P5D",
	"warning": "",
	"wiki_url": "",
	"category": "3D View",
	}

import os
import sys
import importlib

dir = os.path.dirname(__file__)	 # Set filepath to current directory
sys.path.append(dir)

import body
import face
import hair
import shared
import eyetracking
import texture_export
import merge

if "bpy" in locals():  # Allow modules to be reloaded
	from importlib import reload
	reload(body)
	reload(face)
	reload(hair)
	reload(shared)
	reload(eyetracking)
	reload(texture_export)
	reload(merge)
else:
	from .body import *
	from .face import *
	from .hair import *
	from .shared import *
	from .eyetracking import *
	from .texture_export import *
	from .merge import *

import bpy
from bpy import (data, context, ops)
from bpy_extras.io_utils import ImportHelper
from bpy.types import (Panel, 
					   Operator,
					   PropertyGroup,
					   Scene,
					   Object,
					   EditBone,
					   )
from bpy.props import (StringProperty, 
					   BoolProperty,
					   FloatProperty,
					   )

# GLOBAL VARIABLES
fixer_enabled = False


# ------------------------------------------------------------------ #
# CUSTOM PROPERTIES
# ------------------------------------------------------------------ #
class CustomProps(PropertyGroup):
	Object.vrc_offset = FloatProperty(default = 0.08)  # Offset armature to prevent feet sinking into ground


class UIProps(PropertyGroup):		
	# Remove Outline
	Scene.remove_outline = BoolProperty(
		name = "remove_outline",
		description = "Delete outlines and eye shadow",
		default = True,
		)
		
	# Fix Feet
	Scene.fix_feet = BoolProperty(
		name = "fix_feet",
		description = "Adjust armature so your avatar doesn't sink into the ground \nDelete toe bones (these can cause you to stand weirdly)",
		default = True,
		)	

	# Join Mesh
	Scene.join_mesh = BoolProperty(
		name = "join_mesh",
		description = "Enabled:   Joins objects into a single mesh \nDisabled:  Separates mesh by materials",
		default = True,
		)

	# ------------------------------------------------------------------ #	
	# LAYOUT
	# Toggle Checkbox Layout
	Scene.toggle_checkbox = BoolProperty(
		name = "toggle_checkbox",
		description = "Show options as full-size named checkboxes",
		default = False,
		)

	# Toggle Manual Fix Buttons
	Scene.toggle_manual_fix = BoolProperty(
		name = "toggle_manual_fix",
		description = "Show options for manually fixing individual parts",
		default = False,
		)

	# Apply Fix is enabled
	Scene.fixer_enabled = BoolProperty(
		name = "fixer_enabled",
		description = "Auto-Fix is enabled",
		default = False,
		)


# ------------------------------------------------------------------ #
# LAYOUT
# ------------------------------------------------------------------ #
class FixerLayout(Panel):
	"""Fixes the face mesh of P3D/P5D models for use in VRChat"""
	bl_label = "Persona Quick Fixer"
	bl_idname = "fixer.layout"
	bl_space_type = 'VIEW_3D'
	bl_region_type = 'UI'

	# Add a panel to the UI panel in the 3D View	
	def draw(self, context):
	
		# Import/Export Buttons
		layout = self.layout
		c = layout.column(align=True)	
		r = c.row(align=True)
		r.scale_y = 1.5
		# layout.row()  # Line space
		
		if len(context.scene.objects) > 0:
			r.operator("fixer.import", text="Import", icon="FILESEL")		
			r.operator("fixer.export", text="Export", icon="LOAD_FACTORY")
		else:
			r.operator("fixer.import", text="Import FBX", icon="FILESEL")
		
		# ------------------------------------------------------------------ #
		# Checkboxes
		c = layout.column(align=True)
		c.prop(context.scene, "remove_outline", text='Remove Outline')
		c.prop(context.scene, "fix_feet", text='Fix Feet')
		c.prop(context.scene, "join_mesh", text='Join Mesh')

		# ------------------------------------------------------------------ #
		# Auto-Fix
		fixer_c = layout.column(align=True)
		split = fixer_c.split(percentage=0.85, align=True)
		auto_c = split.column(align=True)
		auto_c.scale_y = 1.4
		auto_c.operator("fixer.fix_model", text="Apply Fix", icon="MODIFIER")	
		
		# Individual Fix Toggle
		c = split.column(align=True)
		c.scale_y = auto_c.scale_y
		c.prop(context.scene, "toggle_manual_fix", text="", icon="COLLAPSEMENU")
		
		# Individual Fix
		if context.scene.toggle_manual_fix:
			r = fixer_c.row(align=True)
			r.scale_y = 1.2
			r.operator("fixer.fix_face", text="Face")
			r.operator("fixer.fix_body", text="Body")
			r.operator("fixer.fix_hair", text="Hair")

			col = layout.column(align=True)  # Add gap below buttons								
		else:
			col = fixer_c.column(align=True)
		
		# ------------------------------------------------------------------ #
		# Merge Armatures
		c = col.column(align=True)
		c.scale_y = 1.4
		c.operator("fixer.armature_merge", text="Merge Armatures", icon="OUTLINER_OB_ARMATURE")

		# Export Textures
		c = col.column(align=True)
		c.scale_y = 1.4
		c.operator("fixer.texture_export", text="Export Textures", icon="TEXTURE")
		

# ------------------------------------------------------------------ #
# RUN FIXER
# ------------------------------------------------------------------ #
class FixerModel(Operator):
	"""Apply fix to each body part"""
	bl_label = "Run Auto-Fix"
	bl_idname = "fixer.fix_model"
	bl_optons = {'REGISTER', 'UNDO'}
	
	op_run = 'AUTO'	# Declare which operator is running
	armature = None	

	# Check for visible armatures
	@classmethod
	def poll(cls, context):
		i = len([x for x in context.visible_objects if x.type=="ARMATURE"])
		global fixer_enabled

		if i > 0: fixer_enabled = True
		else:	  fixer_enabled = False		
		return fixer_enabled	

	# Run Fix
	def execute(self, context):
		print("\n>>> START: AUTO-FIX")
		body, status_body = fix_body(self, context)		# Body Fix
		face, status_face = fix_face(self, context)		# Face Fix
		hair, status_hair = fix_hair(self, context)		# Hair Fix -- Hair must be last to avoid parenting face mesh to hair armature

		# Set armatures to Pose Position
		for arm in filter(lambda x: x is not None, [body, face, hair]):  
			shared.armature_pose(arm)	

		# Set armature in CATS -- for testing eye tracking	
		if face is not None: 
			shared.set_cats_armature(face)	 
		
		# All fixers cancelled
		if body == face == hair == None:
			if "MULTI" in (status_body, status_face, status_hair):
				# Multiple armatures
				self.report({"ERROR"}, "Multiple armatures found \nMake sure there is only one of each body part")
			else:
				# No armatures
				self.report({"ERROR"}, "No armatures found")		
			return {"CANCELLED"}

		# Model is already fixed and un-modified
		elif all(x not in ["MODIFIED", "RUN"] for x in [status_body, status_face, status_hair ]):
			shared.messagebox(self, title="Cancelled", 
								 	message="Model has already been fixed",
								 	report="WARNING",
								 	popup=True,
								 	)
			return {"CANCELLED"}

		# Fixer was run
		else:
			print("\nCONTINUE AUTO-FIX")

			#TODO Specify which armature to set for CATS / rename

			if "MODIFIED" in [status_body, status_face, status_hair]:	# If model changed...
				armature = self.armature
				shared.redo_cats(self, context, armature)	# Re-create eye-tracking and visemes, if single mesh
				shared.rename_armature(context, armature)	# Rename armature and set to pose position

			bpy.ops.ed.undo_push(message="Run Auto-Fix")	# Add undo to the stack
			self.report({"INFO"}, "Auto-Fix Successful!")
			return {"FINISHED"}

			
# Face Fixer
class FixerFace(Operator):
	"""Run fix for Face only"""
	bl_label = "Run Face Fix"
	bl_idname = "fixer.fix_face"
	bl_optons = {'REGISTER', "UNDO"}
	
	op_run = 'FACE'	# Declare which operator is running
	armature = None

	# Check fixer is enabled
	@classmethod
	def poll(cls, context):
		return fixer_enabled
	
	# Run Fix	
	def execute(self, context):
		armature, status = fix_face(self, context)
		bpy.ops.ed.undo_push(message="Run Face Fix")  # Add undo to the stack
		
		if armature is None or status == "FIXED":
			return {"CANCELLED"}
		else:
			return {"FINISHED"}

		
# Body Fixer
class FixerBody(Operator):
	"""Run fix for Body only"""
	bl_label = "Run Body Fix"
	bl_idname = "fixer.fix_body"
	bl_optons = {'REGISTER', "UNDO"}
	
	op_run = 'BODY'	# Declare which operator is running	
	armature = None

	# Check fixer is enabled
	@classmethod
	def poll(cls, context):
		return fixer_enabled
	
	# Run Fix	
	def execute(self, context):
		armature, status = fix_body(self, context)
		bpy.ops.ed.undo_push(message="Run Body Fix")  # Add undo to the stack
		
		if armature is None or status == "FIXED":
			return {"CANCELLED"}
		else:
			return {"FINISHED"}
		
		
# Hair Fixer
class FixerHair(Operator):
	"""Run fix for Hair only"""
	bl_label = "Run Hair Fix"
	bl_idname = "fixer.fix_hair"
	bl_optons = {'REGISTER', "UNDO"}
	
	op_run = 'HAIR'	# Declare which operator is running	
	armature = None

	# Check fixer is enabled
	@classmethod
	def poll(cls, context):
		return fixer_enabled
	
	# Run Fix	
	def execute(self, context):
		armature, status = fix_hair(self, context)
		bpy.ops.ed.undo_push(message="Run Hair Fix")  # Add undo to the stack
		
		if armature is None or status == "FIXED":
			return {"CANCELLED"}
		else:
			return {"FINISHED"}

		
# ------------------------------------------------------------------ #
# MERGE ARMATURES
# ------------------------------------------------------------------ #
class FixerMergeArmature(Operator):
	"""Automatically fix model parts and merge into a single armature"""
	bl_label = "Merge Armatures"
	bl_idname = "fixer.armature_merge"
	bl_optons = {'REGISTER', 'UNDO'}
	
	op_run = 'MERGE'	# Declare which operator is running
	armature = None
	
	# Check for multiple armatures
	@classmethod
	def poll(cls, context):
		i = len([x for x in context.visible_objects if x.type=="ARMATURE"])
		return i > 1
	
	# Run Merge
	def execute(self, context):
		x = merge_armatures(self, context)
		bpy.ops.ed.undo_push(message="Merge Armatures")  # Add undo to the stack
		
		if x:
			self.report({"INFO"}, "Merged Armatures")
			return {"FINISHED"}
		else:
			return {"CANCELLED"}
		

# ------------------------------------------------------------------ #
# EXPORT TEXTURES
# ------------------------------------------------------------------ #
class FixerTextureExport(Operator, ImportHelper):
	# Opens file browser
	bl_label = "Export Textures"
	bl_idname = "fixer.texture_export"
	bl_optons = {'REGISTER', 'INTERNAL', 'UNDO'}
	bl_description = "Export textures as PNG (.png) images. Applies to visible objects only.\n" \
					 "Update model to use new PNG images (optional)"
	
	# Custom options
	do_overwrite = BoolProperty(
				name='Overwrite Existing',
				description='Overwrite existing images in destination folder',
				default=False,
				)
	
	update_tex = BoolProperty(
				name='Update Textures',
				description='Update model textures to use PNG images',
				default=True,
				)
	
	is_relative = BoolProperty(
				name='Relative Path',
				description='Save filepath relative to this blend file',
				default=True,
				)

	# Check visible objects for textures
	@classmethod
	def poll(cls, context):
		if len(context.visible_objects) > 0:			
			for obj in filter(lambda x: x.type=="MESH",	context.visible_objects):	# Check each mesh object for material
				try:
					for mat in obj.data.materials:				 # Check each material for texture
						if len(mat.texture_slots.items()) > 0:	 # Texture found						
							return True
				except (AttributeError):  # Error: Object has blank material
					continue
		else:
			return False

	# Export textures as PNG
	def execute(self, context):
		print("\n>>> START: EXPORT TEXTURES")
		x = texture_export(self, context)
		bpy.ops.ed.undo_push(message="Export Textures")  # Add undo to the stack
		
		if x:
			shared.messagebox(self, title="Finished",
								 	message="Finished: Texture Export",
								 	report="INFO")
			return {"FINISHED"}
		else:
			return {"CANCELLED"}


# ------------------------------------------------------------------ #
# IMPORT/EXPORT
# ------------------------------------------------------------------ #
# Open file browser to import FBX
class FixerImport(Operator):
	"""Import FBX (.fbx)"""
	bl_label = "Import FBX"
	bl_idname = "fixer.import"
	
	def execute(self, context):
		print(">>> IMPORT FBX")		
		ops.import_scene.fbx('INVOKE_DEFAULT',
							  automatic_bone_orientation=True)
		self.report({"INFO"}, "Imported: FBX Model")
		return {"FINISHED"}


# Open file browser to export FBX
class FixerExport(Operator):
	"""Export FBX (.fbx) \n(Uses CATS export function)"""
	bl_label = "Export FBX"
	bl_idname = "fixer.export"
	
	# Export using CATS export function
	def execute(self, context):
		print(">>> EXPORT FBX")
		bpy.ops.cats_importer.export_model(action='CHECK')
		self.report({"INFO"}, "Exported: FBX Model")
		return {"FINISHED"}


# ------------------------------------------------------------------ #
def register():
	bpy.utils.register_module(__name__)

def unregister():
	bpy.utils.unregister_module(__name__)

if __name__ == "__main__":
	register()