import bpy
import face
import body
import hair
import eyetracking

from bpy import (data, context, ops)
from math import radians
from mathutils import Vector

from body import *
from hair import *
from face import *
from eyetracking import *


#TODO Fix hair armature error when searching for second bone_name in list - need if statement
	

# ------------------------------------------------------------------ #
# CUSTOM MESSAGE POPUP
# ------------------------------------------------------------------ #
def messagebox(self, title="Info", message="", icon="INFO", report="OPERATOR", popup=False):
	def draw(self, context):
		self.layout.label(message, icon=icon)			# Draw messagebox
	
	print(message)							# Print to console
	self.report({report.upper()}, message)	# Display internal report

	# Display custom messagebox
	if popup or report.upper() == "ERROR":
		context.window_manager.popup_menu(draw, title="Report: " + title)


# Messagebox with multiple icons
def messagebox_multi(self, title="Info", message={}, report="OPERATOR", popup=False):
	def draw(self, context):
		layout = self.layout
		for icon, text in message.items():
			layout.label(text[0], icon=icon)	# Draw icon for first item
			for x in text[1:]:					
				layout.label(x)					# Draw each item
	
	# Print message
	for icon, text in message.items():
		for x in text:
			print(x)							# Print to console
			self.report({report.upper()}, x)	# Display internal report

	# Display custom messagebox
	if popup or report.upper() == "ERROR":
		context.window_manager.popup_menu(draw, title="Report: " + title)
	

# ------------------------------------------------------------------ #
# DETERMINE ARMATURE
# ------------------------------------------------------------------ #
def find_armature(self, context, body_part):
	body_part_name = body_part.capitalize()
	run_manual = False if self.op_run in ["AUTO", "MERGE"] else True
	print("Run manually: " + str(run_manual))
	
	# Search visible objects for Armature
	mode_object()
	select_none()
	armature, need_fix, count = test_armature(body_part)	# Test armature to determine body part and if it needs fixing

	# No armatures found
	if count == 0:
		if run_manual:  # Display warning if running manual fix
			print("Armature not found: " + body_part_name)
			self.report({"ERROR"}, body_part_name + " armature not found")
		return None, "NONE"

	# Single armature found
	elif count == 1:
		set_active(armature) 			# Set armature as active object
		set_cats_armature(armature)		# Set armature in CATS
		self.armature = armature		# Store armature in class property
		
		# Success: Armature needs fixing
		if need_fix:
			print("Armature found: " + armature.name)	# Return armature
			return armature, "RUN"

		# Armature already fixed
		else:
			print("Armature already fixed: " + armature.name)
			armature_pose(armature)											# Set armature to Pose Position
			armature_visible(armature)										# Set armature to wireframe
			fix_materials(context, armature)								# Fix materials
			toes_changed = remove_toe_bones(context, armature, "OBJECT")	# Remove toe bones (optional)
			outline_changed = run_remove_outline(self, context, armature)	# Remove Outlines -- must do first
			mesh_changed = run_join_mesh(self, context, armature)			# Join Mesh
			# select_none()													# Clear selection
			# armature.select = True											# Select armature
			
			# Armature has been modified
			if mesh_changed or outline_changed or toes_changed:
				return armature, "MODIFIED"
			
			# Armature un-modified
			else:
				if run_manual:  # Display warning if running manual fix
					messagebox(self, title = "Cancelled", 
								  	 message = body_part_name + " armature is already fixed", 
								  	 report = "warning",
								  	 popup = True,
								  	 )		
				return armature, "FIXED"

		
	# Multiple armatures found
	else:
		messagebox(self, title = "Error",
						 message = "Multiple '" + body_part_name + "' armatures found",
						 report = "error",
						 )
		return None, "MULTI"
		

# ------------------------------------------------------------------ #
# Determine armature by searching bones
def test_armature(body_part):
	if   body_part == "BODY": bone_name = ["Hips"]
	elif body_part == "FACE": bone_name = ["R_eye"]	
	elif body_part == "HAIR": bone_name = ["hair", "h00"]
		
	armature = None
	need_fix = False
	count = 0

	# Search armatures for specific bone
	for name in bone_name:
		if count == 0:										# Only search if bone not already found
			print("Searching for bone: " + name + " ...")
			for arm in filter(lambda x: x.type=="ARMATURE", bpy.context.visible_objects):	# Search visible objects for armatures
				print("Searching in: " + arm.name + " ...")		
				for bone in arm.data.bones:					# Search bones in armature			
					if name in bone.name:					# Check if bone name contains string
						count += 1
						if count == 1:
							print("Bone found: " + bone.name)
							need_fix = test_need_fix(arm)  	# Test if armature needs fix
							armature = arm
							break
						elif count > 1:
							return None, None, count		# Return if multiple armatures found
				else:
					print("Bone not found!")

	return armature, need_fix, count
		

# ------------------------------------------------------------------ #
# Test if armature needs fixing
def test_need_fix(armature):
	print("Testing for fix: " + armature.name)
	need_fix = False
	
	# Test scale x, y, z
	for i in range(2):
		if round(armature.scale[i], 4) == 0.0254:	# Default import scale = 0.0254
			need_fix = True
		else:
			return False

	# Print report
	if need_fix:
		print("Armature needs fixing!")
		
	return need_fix
	

# ------------------------------------------------------------------ #
# ARMATURE FUNCTIONS
# ------------------------------------------------------------------ #
# Fix Armature visibility
def armature_visible(armature):	
	# Set to wireframe and x-ray
	armature.draw_type = 'WIRE'
	armature.show_x_ray = True
	print("Set armature to wireframe")


# Apply scale and rotation to selected objects
def apply_transform(armature):				
	# Apply scale and rotation to selection objects
	armature.select = True
	ops.object.transform_apply(location=False, rotation=True, scale=True)
	select_none()
	armature.select = True		
	print("Applied rotation and scale")
	

# Fix rotation of loose objects
def fix_rotation(obj):
	obj.select = True
	obj.rotation_euler = [radians(90), 0, 0]
	obj.scale = [0.0254, 0.0254, 0.0254]
	print("Fixed rotation for: " + obj.name)
	

# Remove mesh bones
def remove_mesh_bones(edit_bones):
	for bone in edit_bones:	
		name = bone.name.lower()
		if "mesh_" in name:
			for child in bone.children_recursive:	# Remove child bones
				edit_bones.remove(child)
			edit_bones.remove(bone)					# Remove parent bone
			break
	print('Removed mesh bones')
	

# Remove toe bones by merging weight to parent  (optional)
def remove_toe_bones(context, armature, mode):
	scene = bpy.context.scene
	edit_bones = armature.data.edit_bones	
	toe_bones = []
	remove = False

	if scene.fix_feet:
		print('Removing toe bones...')				
		scene.objects.active = armature	 		# Make armature the active object

		for bone in armature.data.bones:		# Search aramture for toe bones
			if "toe" in bone.name.lower():		# Use lowercase for easier searching
				toe_bones.append(bone.name)		# Store toe bone

		if toe_bones != []:
			mode_edit()							# Edit Mode
			select_none_armature()				# Deselect armature

			offset = edit_bones[toe_bones[0]].tail.z	 # Store toe offset to custom property
			armature.vrc_offset = offset				
			print("Toe offset: " + str(offset))

			for toe in toe_bones:
				edit_bones[toe].select = True			 # Select toe bone
				remove = True							 # Declare changes are made
				print('Removed toe bone: ' + toe)

			if remove:
				bpy.ops.cats_manual.merge_weights()  # Merge weights of toes to parent
		
	if mode == "OBJECT": mode_object()					 # Return to Object Mode
	return remove
	

# ------------------------------------------------------------------ #
# MESH FUNCTIONS
# ------------------------------------------------------------------ #
# Re-parent loose objects
def fix_armature_objects(self, context, armature, fixer, exclude = []):
	print("Searching for loose objects")	
	list_objects = []

	# Search for loose objects
	for obj in context.visible_objects:
		if (obj.name.startswith("m_") and	# Check for persona mesh objects
			obj.parent is None				# No parent
			):

			try:
				mat_name = obj.data.materials[0].name			 # Store name of first material

				if any(x in mat_name for x in ["hat", "hair"]):	 # Hair accessories parent to hair
					if fixer == "hair":
						list_objects.append(obj)

				elif fixer in mat_name:							 # Material contains same name as body part being fixed
					list_objects.append(obj)

				elif search_bone_name(armature, obj.name):		 # Armature contains corresponding bone
					list_objects.append(obj)

				elif exclude != []:
					if any(s in obj.name for s in exclude) is False:	# Object name is not in excluded list (Body Fix)
						list_objects.append(obj)

			except (IndexError, AttributeError):  # If object has no material
				print("ERROR: Object ' " + obj.name + " has no material")
				continue
	
	# Fix object rotation
	for obj in list_objects:
		fix_rotation(obj)
				
	# Parent selected objects to armature
	ops.object.parent_set(type="ARMATURE")
	print("Parented loose objects to armature: " + str(len(list_objects)))
	
	# Fix objects in armature
	for obj in armature.children:
		obj.select = True
	
	armature_visible(armature)			# Set armature to wireframe and x-ray
	apply_transform(armature)			# Apply rotation and scale to selected objects
	fix_materials(context, armature)	# Fix material brightness
	
	print("Applied transform & fixed materials")
	return list_objects


# Search armature for bone corresponding to mesh
def search_bone_name(armature, obj_name):
	bone_name = obj_name.split("_Mesh", 1)[0]	# Split object name at "_Mesh" and take first element
	# print("Searching for bone: " + bone_name)	
	found = False
	if armature.data.bones.find(bone_name) != -1:
		found = True	
	# print("Found bone: " + str(found))
	return found


# ------------------------------------------------------------------ #
# Add weight to loose objects, if there is a corresponding bone
def add_weight(accs, edit_bones, hair):
	for obj in accs:
		if len(obj.vertex_groups) == 0:					# If there are no existing vertex groups in object
			bone_name = obj.name.split("_Mesh", 1)[0]	# Split the object name into 2 parts and take first body part
			
			# Matching bone is found
			if edit_bones.find(bone_name) != -1:			
				add_vertex(context, obj, bone_name)		# Add vertex group with weight 1.0
			
			# Matching bone not found and "hair" is True
			elif hair:
				add_vertex(context, obj, "Head")		# Add "Head" vertex group with weight 1.0
				

# Create vertex groups
def add_vertex(context, obj, string, weight = 1.0):
	set_active(obj) # Set object as active
	
	if obj.vertex_groups.find(string) == -1:		# Vertex group doesn't already exist
		vg = obj.vertex_groups.new(name = string)	# Create new vertex group
		verts = []
		
		for v in obj.data.vertices:		# Store all vertices
			verts.append(v.index)
		
		vg.add(verts, weight, 'ADD')	# Add weight to all vertices
		
		print("Added vertex group '" + string + "' to object '" + obj.name + "'")
		

# ------------------------------------------------------------------ #
# MATERIAL FUNCTIONS
# ------------------------------------------------------------------ #
# Set materials to shadeless and fix brightness
def fix_materials(context, armature):
	for x in armature.children:
		for mat in x.data.materials:			
			mat.use_shadeless = True			# Set material to shadeless
			fix_face_texture(mat)				# Fix face textures		
			if not test_outline(mat):				
				mat.diffuse_color = (1, 1, 1)	# Increase brightness, if not outline
	print("Set materials to shadeless")


# Set face material to use single texture
def fix_face_texture(material):
	D = bpy.data
	slot = material.texture_slots[0]  # First texture slot

	if "face" in material.name:
		for index, tex in enumerate(material.texture_slots):	# Search texure slots
			try:	
				image = bpy.path.basename(tex.texture.image.filepath)	# Store image filename
			except AttributeError:
				continue

			if "_tex" in image:								# Search image filename for "_tex" string
				slot.texture = D.textures[tex.name]			# Assign face texture to FIRST texture slot
				slot.use_map_color_diffuse = True			# Fix texture colour
				slot.blend_type = "MIX"						# Set blend mode to "Mix"
				if index > 0:
					material.texture_slots.clear(index)		# Clear CURRENT texture slot

		print("Fixed face texture in: " + material.name)
		

# Test for outline mesh
def test_outline(mat):
	# Check if material is outline or eye shadow
	if any(str in mat.name for str in ["outline", "eye_sh"]):
		return True
	else:
		return False


# ------------------------------------------------------------------ #
# REDO CATS
# ------------------------------------------------------------------ #
# Redo eye-tracking and visemes using CATS
def redo_cats(self, context, armature):
	print("Redo eye-tracking and visemes using CATS")
	context.scene.armature = armature.name									# Set CATS to correct armature
	if context.scene.join_mesh:
		if test_hierarchy(armature):		
			eye_mesh, face_mesh  = find_head_mesh(armature)					# Find eye and face mesh
			if all([eye_mesh, face_mesh]):									# If found...
				create_cats_visemes(self, context, armature, face_mesh)		# Redo visemes
				set_cats_armature(armature)									# Set armature in CATS
				set_cats_eyes(context, eye_mesh)							# Reset CATS eye tracking settings
				remove_eye_vertex_groups(eye_mesh)							# Remove existing vertex groups to avoid duplicates
				bpy.ops.cats_eyes.create_eye_tracking()						# Redo eye tracking using CATS
				set_cats_eyes(context, eye_mesh)							# Reset CATS eye tracking settings								


# Test armature hierarchy
def test_hierarchy(armature):	
	hierarchy = ['Hips', 'Spine', 'Chest', 'Neck', 'Head']
	bone = armature.data.bones
	prev = None
	is_correct = False
	print('Testing hierachy of: ' + armature.name)

	for bone_name in hierarchy:
		try:
			if bone[bone_name].parent == prev:	# Check current bone parent is previous
				prev = bone[bone_name]			# Store current bone as previous
				is_correct = True
			else:
				break

		except KeyError:  # Error: Hips not found
			break  
	
	if is_correct:
		print('Hierarchy is correct')
	else:
		print('Hierarchy is incomplete')
		
	return is_correct


# Find face and eye mesh
def find_head_mesh(armature):
	eye_mesh = None
	face_mesh = None

	for mesh in filter(lambda x: x.type=="MESH", armature.children):	# Search mesh objects in armatures
		shape_keys = mesh.data.shape_keys

		# Find eye mesh by searching for "R_eye" vertex groups
		if mesh.vertex_groups.find("R_eye") != -1:				 					 
			eye_mesh = mesh
			print("Eye mesh: " + eye_mesh.name)  # Found eyes

		# Find face mesh by searching for "Oh" shape keys
		if shape_keys is not None:
			if shape_keys.key_blocks.find("Oh") != -1:
				face_mesh = mesh
				print("Face mesh: " + face_mesh.name)	# Found face

	if eye_mesh is None:	print("Failed to find eye mesh")		
	elif face_mesh is None:	print("Failed to find face mesh")

	return eye_mesh, face_mesh
	
	
# ------------------------------------------------------------------ #
# FINISH FIXER
# ------------------------------------------------------------------ #
def finish_fixer(self, context, armature, body_part = ""):
	print("Finishing fix for: " + armature.name)
	set_cats_armature(armature)						# Set armature in CATS -- required for joining mesh
	run_remove_outline(self, context, armature)		# Remove outlines -- must do first
	run_join_mesh(self, context, armature)  		# Join mesh
	rename_armature(context, armature, body_part)	# Rename armature
	select_none()			 						# Deselect all

	print(">>> FINISHED " + body_part.upper() + " FIX\n")

	if body_part != "":  # Post report if run manually
		messagebox(self, title = "Finished",
					  	 message = body_part.capitalize() + " fix sucessful!",
					  	 report = "INFO",
					  	 )
		
# ------------------------------------------------------------------ #
# Delete outlines
def run_remove_outline(self, context, armature):
	if context.scene.remove_outline:
		print("Removing outlines...")
		del_out = False

		if len(armature.children) != 1:	 				# Don't remove outline if armature only has a single mesh
			for child in armature.children:				# Search each child in armature
				for material in child.data.materials:	
					if not test_outline(material):		# Test each material is an outline
						break  							# Break if not outline material
				else:
					name = child.name					# Store mesh name for print log
					bpy.data.objects.remove(child)  	# Delete mesh if all materials are an outline
					del_out = True
					print("Removed outline: " + name)
			
			mode_edit()

			edit_bones = armature.data.edit_bones			
			for bone in edit_bones:
				if "outline" in bone.name:
					edit_bones.remove(bone)				# Remove outline bones
					
			mode_object()
		
		if del_out: self.report({"OPERATOR"}, "Removed outlines")
		else:		print("No outlines removed")
		return del_out	
	
# ------------------------------------------------------------------ #
# Join mesh using CATS
def run_join_mesh(self, context, armature):
	print("Running join mesh...")
	x = len(armature.children)
	mesh_changed = False
	
	# Join mesh -- only if multiple meshes in armature
	if context.scene.join_mesh:
		if x > 1:															
			bpy.ops.cats_manual.join_meshes()
			mesh_changed = True
			messagebox(self, message="Joined mesh", icon="FILE_TICK")
	
	# Separate by materials -- only if single mesh in armature
	else:
		if x == 1:															
			set_active(armature.children[0])		 		 # Set children as active objects -- avoids errors from CATS
			bpy.ops.cats_manual.separate_by_materials()	 	 # Separate by material
			run_remove_outline(self, context, armature)		 # Remove outlines, if enabled
			set_active(armature) 							 # Set armature as active object
			mesh_changed = True
			messagebox(self, message="Separated mesh by material", icon="FILE_TICK")
	
	if not mesh_changed: print('No change to mesh')		
	return mesh_changed	
	
# ------------------------------------------------------------------ #
# Rename Armature
def rename_armature(context, armature, body_part=""):
	set_active(armature) 	# Set armature as active object
	mode_object()			# Set to OBJECT MODE
	
	body_part = body_part if body_part=="" else (" " + body_part)	# If body part is not blank add space before

	armature.name = "Armature" + body_part		# Rename armature
	armature.data.name ="Armature" + body_part
	
	# context.scene.armature = armature.name		# Set cats to use correct armature
	# armature.select = True						# Select armature
	armature_pose(armature)	 					# Set armature to Pose Position
	print("Renamed armature: " + armature.name)


# ------------------------------------------------------------------ #
# COMMON FUNCTIONS
# ------------------------------------------------------------------ #
def select_all():
	ops.object.select_all(action="SELECT")		# Selection in Object Mode
	
def select_none():
	ops.object.select_all(action="DESELECT")

def select_all_armature():
	ops.armature.select_all(action='SELECT')	# Selection in Edit Mode

def select_none_armature():
	ops.armature.select_all(action='DESELECT')

def mode_object():
	if bpy.ops.object.mode_set.poll():
		ops.object.mode_set(mode='OBJECT')		# Object Mode
		print("[OBJECT MODE]")
	
def mode_edit():
	if bpy.ops.object.mode_set.poll():
		ops.object.mode_set(mode='EDIT')		# Edit Mode
		print("[EDIT MODE]")

def set_active(obj):
	bpy.context.scene.objects.active = obj		# Set Active Object
	print("ACTIVE OBJECT: " + obj.name)

def set_cats_armature(armature):
	bpy.context.scene.armature = armature.name	# Set Armature in CATS
	print("CATS Armature: " + armature.name)

# Set armature to Pose Position
def armature_pose(armature):  
	armature.data.pose_position = "POSE"		# Set Armature to Pose Position
	print("Pose Position: " + armature.name)