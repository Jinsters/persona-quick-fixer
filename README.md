# Jinsters - Persona Quick Fixer
A blender addon to convert character models from ***Persona 3: Dancing Moon Night*** and ***Persona 5: Dancing Star Night*** for use as avatars in VRChat. Quickly mix and match body parts to make your perfect waifu or husbando.

The example below was made from Futaba's body, Mitsuru's face and P3 Makoto's hair in red:

![](screenshots/example_mix_n_match.png)

<br>

## Compatibility
- ***Version 0.5.8 requires Cat's Blender Plugin 0.12.2***
- If you're using an earlier version of CATS you will need version 0.5.7, it can be found in the "releases" folder

<br>

## Notes
- ***This addon may be very buggy. I'm not a programmer. I don't really know what I'm doing.***
- It's basically a glorified macro, most of the complex function are done by CATS. So a big thanks to everyone who helps with that project! <3
- It's only designed to work with P3D/P5D models in FBX format exported directly from 3ds Max

## Requirements
- [Blender 2.79](https://www.blender.org/download/)
- [Cats Blender Plugin](https://github.com/michaeldegroot/cats-blender-plugin) - *Version 0.12.2 onwards*
- [3ds Max](https://www.autodesk.com/education/free-software/3ds-max) - *You can get it free for 3 years with a student licence (3ds Max 2016 recommended)*
- [GMD Model Import Maxscript by TGE](https://amicitia.github.io/tools/p5) - *For extracting Persona models in 3ds Max*

## Download
- #### Version 0.5.8 - [persona-quick-fixer-0.5.8.zip](https://gitlab.com/Jinsters/persona-quick-fixer/raw/master/releases/persona-quick-fixer-0.5.8.zip)

<br>

## Features
#### Import / Export FBX:
- Import using optimal settings for FBX models
- Export using optimal settings for VRChat

#### Quick Fixer
- Automatically fixes each body part:
    - **FACE**
        - Aligns, scales and weight paints the face
        - Parents mesh objects to the armature
        - Deletes excess morph target mesh objects
        - Set up shape keys for ***lipsync*** and ***expressions***
        - Adds ***eye tracking***
    - **BODY**
        - Re-parents roll bones (fixes messed up arms and ankles)
        - Merges/delete excess bones that aren't used by VRChat
        - Renames bones to match the naming convention used by CATS
        - Parents and weight paints accessories to the armature
        - Combines similar materials and fixes brightness
        - *(Optional) Re-position the armature so your avatar feet don't sink into the ground*
        - *(Optional) Remove toe bones, since these can make you walk weirdly in-game*
    - **HAIR**
        - Fixes armature scale and rotation
        - Parents and weight paints accessories to the armature
        - Renames head and neck bone to match the naming convention used by CATS

#### Merge Armatures
- Automatically fixes the individual body parts then merges them into a single armature
- Parts can be ***mixed & matched*** from different characters, scaled, rotated and re-positioned

#### Export Textures as PNG
- Exports model textures as PNG images, coverting from the original DDS format
- Automatically update the model to use the exported PNG textures

<br>

## Instructions
Installing this addon adds a panel to the Tool Properties panel on the ***right-hand side*** of the 3D View:  
(Press ***N*** to display the panel if it is hidden)

![](screenshots/screenshot_persona_quick_fixer.png)

<br>
    
### Step 1: Importing
![](screenshots/guide_import_export.png)
- Export the models you want from 3ds Max in ***separate FBX files*** for Body / Face / Hair
- Import the parts into blender using the ***'Import'*** button:
    - Parts can be mix & matched from different characters
    - The addon is only designed to work with ***one of each body part***

<br>

### Step 2: Fixing
![](screenshots/guide_fixer.png)
- Select the options you want to use:
    - **REMOVE OUTLINE**
        - Removes any outline and eye shadow meshes -- *Runs the first time you click apply*
    - **FIX FEET**
        - This offsets the armature so your feet don't sink into the ground -- *Runs when you merge armatures*
        - Removes toe bones, since these can make you walk weirdly in-game -- *Runs the first time you click apply*
    - **JOIN MESH**
        - Checked -- Joins objects into a single mesh under their respective armatures
        - Uncheck -- Separates mesh by materials
        - Runs each time you click apply:
            - If you click 'Apply Fix' if will be applied to all the body parts
            - If you only want to change one part then use the individual fix buttons in the dropdown menu

- Click ***'Apply Fix'*** to automatically fix each body using the chosen options:
    - The fix only operates on visible objects; if you want to have multiple characters in a single blend file you can separate them across layers
    - There's a drop down menu next to the 'Apply Fix' button. This displays buttons to ***fix individual body parts*** without altering the others
        #### ![](screenshots/guide_individual_fix.png)
- This addon ***disables the default VRChat blink*** animation (I think it looks ugly). You can re-enable blinking using CATS or use a custom blink animation in Unity.
- Shape keys for the face should be setup automatically for lipsync and expressions:
    - The expressions should be named appropriately but sometimes the ***name doesn't match the expression***. You can just rename to them to whatever you want.
    - Some of the shape keys will just be ***numbers instead of a name***. This is because the models don't all have the same expressions. You can delete/ignore them if they don't seem to do anything

<br>

### Step 3: Merging Armatures
![](screenshots/guide_merge_armatures.png)
- Click ***'Merge Armatures'*** to merge each body part into a single armature:
    - Only works with ***one of each body part***, i.e. if there's more than one of each part visible you will get an error
        #### ![](screenshots/guide_error_multi.png)
- If you are mixing parts from different characters you should position/scale them ***before merging the armatures.***
- This function will automatically ***fix all body parts before merging.*** If you don't plan on editing the individual body parts you can skip 'Step 2' and immediately merge armatures.
- Once an armature has be merged it ***cannot be separated*** again.

<br>

### Step 4: Exporting Textures
![](screenshots/guide_export_textures.png)
- By default the models use DDS images files for textures; these will appear washed out in Unity. To look correct they need to be converted to PNG format.
- Click ***'Export Textures'*** to export the textures from all visible objects in PNG format:
    - This opens a file browser where you can choose the save location
    - There are options displayed in the ***bottom left corner***:
        #### ![](screenshots/guide_export_textures_options.png)
        - **OVERWRITE EXISTING**
            - Checked: If the image already exists in the folder it will be overwritten
            - Uncheck: The texture will be skipped during export
        - **UPDATE TEXTURES**
            - Once the textures are exported the model will be updated to use the new PNG images instead of the original DDS
        - **RELATIVE PATH**
            - Whether or not to reference the image file using an absolute filepath or a filepath relative to the current blend file. If you don't know what this means just leave it enabled
    - Click ***'Export Textures'*** in the ***top right corner*** to export to the selected folder 
        #### ![](screenshots/guide_export_textures_button.png)            
- The 'Export Texture' function has only been tested with P3D/P5D models but should work for any model, not just Persona. It should also be able to convert from most image types to PNG.

<br>

### Step 5: Exporting the Model
#### ![](screenshots/guide_import_export.png)
- When you've finished editing the model make sure all the meshes are ***combined into a single mesh***
- Test the model in pose mode to make sure there are ***no weight paint issues***
- Click ***'Export'*** to export the model in FBX format

<br>

## Changelog
#### 0.5.8
- Fixed compatibility issue with CATS 0.12.2

#### 0.5.7
- Fixed error when checking for hair armature
- Changed export button to use CATS export function

#### 0.5.6
- Fixed multiple armatures error being ignored
- Removing outline also removes outline bones
- Changes to merge armatures function:
    - Face used as primary armature
    - No longer use custom properties to identify duplicate bones
    - Fix hierarchy by iterating through bones

#### 0.5.5
- Changed layout again...
- Added polling for manual fix operators
- Fixed error when merging without face or hair armature
- Changed how loose objects are parented to either body or hair:
    - Compare by material, else compare by corresponding bones
    - Hair accessories (e.g. hat) will always parent to hair
- Change 'Remove Toes' to 'Fix Feet':
    - Added function to adjust armature position to stop feet sinking into the ground
    - Optimised deleting toe bones
- Improved Texture Exporter dialog box